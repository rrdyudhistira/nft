import Link from "next/link";
import Navbar from "../components/Navbar";
import Head from "next/head";
import Image from "next/image";

function Home() {
  return (
    <div className="page-wrapper">
      <Head>
        <title>Home</title>
      </Head>

      <div className="top-section">
        <Navbar />
        <section className="hero">
          <div className="container">
            <div className="text-wrapper">
              <h1 className="title">Welcome To</h1>
              <h3 className="title1">
                Digital NFT<span className="title2">Agriculture</span>
              </h3>
              <p className="description">
                DNA (Digital NFT Agriculture) adalah dunia Pertanian Digital
                (Digital Farm) yang digabungan dengan Defi (Decentralize
                Finace).
              </p>
              <div className="links">
                <Link href="/" className="cta">
                  With Paper
                </Link>
                <Link
                  href="https://github.com/rrdyudhistira/nft"
                  passHref
                  className="cta"
                >
                  GitHub
                </Link>
              </div>
            </div>
            <div className="logo">
              <Image src="/favicon.png" width={200} height={200} />
            </div>
          </div>
        </section>
        <div className="wave-container">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#2C2C3B"
              fillOpacity="1"
              d="M0,32L80,37.3C160,43,320,53,480,69.3C640,85,800,107,960,101.3C1120,96,1280,64,1360,48L1440,32L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"
            ></path>
          </svg>
        </div>
      </div>
      <main>
        <div className="img-wrapper">
          <div className="intro">
            <div className="intro-container">
              <div className="intro-h1">
                <h1 className="intro1">Digital Farming</h1>
                <p className="introp1">Pengguna bisa melakukakan aktifitas digital
                  farming dan akan mendapatkan reward dari hasil yang telah pengguna Kelola di digital farming,</p>
              </div>
              <div className="logo1">
                <Image src="/favicon.png" width={200} height={200} />
              </div>
            </div>
            <div className="intro-h12">
              <div className="img">
                <div className="logo2">
                  <Image src="/favicon.png" width={200} height={200} />
                </div>
              </div>
              <div className="descriptionp">
                <h1 className="intro2">Land Map</h1>
                <p className="introp2">Lahan yang sudah kami sediakan untuk di buat sebuah dunia virtual dan sebagai
                  jaminan asset digital yang anda beli secara virtual.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="intro">
        </div>
        <div className="intro-h13">
          <h1 className="intro3">Land Virtual</h1>
          <p className="introp3">kami akan mencetak land virtual ini
            menyesuaikan proyek lahan yang sudah kita
            pegang di dalam kehiduapan secara nyata. Jadi
            hak kepemilikan NFT land digital sama dengan hak
            kepemilikan lahan secara nyata yang sudah kami
            sediakan, lahan tersebut akan di gunakan sebagai
            sarana pertanian pembangunan gedung & wisata
            penghijauan.</p>
          <div className="logo3">
            <Image src="/favicon.png" width={200} height={200} />
          </div>
        </div>
      </main>
      <div>
      </div>
      <div className="img2-wrapper">
        <div className="wave2-container">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#5000ca"
              fillOpacity="1"
              d="M0,32L80,37.3C160,43,320,53,480,69.3C640,85,800,107,960,101.3C1120,96,1280,64,1360,48L1440,32L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"
            ></path>
          </svg>
        </div>
        <div className="ekosistem-container">
          <h1 className="ekosistem-desc">Ekosistem</h1>
        </div>
        <div className="logo-eko">
          <Image src="/ekosistem.jpg" width={400} height={400} />
          <p> </p>
        </div>
      </div>
    </div>
  );
}

export default Home;
