import Link from "next/link";
import { withRouter } from "next/router";

function Navbar({ router }) {
  const navs = [
    { text: "Home", href: "/" },
    { text: "About", href: "#" },
    { text: "Mission", href: "#" },
    { text: "Presale", href: "#" },
    { text: "Values", href: "#" },
    { text: "Roadmap", href: "#" },
    { text: "Mistery Box ", href: "#" },
    { text: "NFT Game", href: "/nft" },
    { text: "Stake", href: "#" },
    {
      text: "NFT Marketplace",
      href: "https://www.binance.com/en/nft/collection/digital-nfts-agriculture-611660844884877312?isBack=1",
    },
    { text: "Trade", href: "https://app.uniswap.org/#/swap?chain=polygon" },
    // { text: '', href: '' },
  ];

  return (
    <nav className="navbar">
      <div className="container-navbar">
        {/* <image src="/favicon.jpg" height="60px" alt="logo">Logo</image> */}
        <ul className="nav-links">
          {navs.map((nav, idx) => (
            <li
              key={`nav-${idx}`}
              className={`nav-item ${router.pathname == nav.href ? "active" : ""
                }`}
            >
              <Link href={nav.href} passHref prefetch={false}>
                {nav.text}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
}

export default withRouter(Navbar);
